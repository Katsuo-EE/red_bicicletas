var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function(req, res){
    res.render('bicicletas/index', {bicis: Bicicleta.allBicis});
}


//dos momentos, solicitud y confirmacion(prop y atributos definidos para la creacion) 
exports.bicicleta_create_get = function(req, res){
    //por convencion usamos el get
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function(req, res){
    //por convencion usamos el post para la creacion en si
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici);
    res.redirect('/bicicletas');
}

//dos momentos, solicitud y confirmacion(prop y atributos definidos para la creacion) 
exports.bicicleta_update_get = function(req, res){
    //por convencion usamos el get

    var bici = Bicicleta.findById(req.params.id);
    res.render('bicicletas/update', {bici});
}

exports.bicicleta_update_post = function(req, res){
    var bici = Bicicleta.findById(req.params.id);
    //por convencion usamos el post para la creacion en si
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.moddelo,
    bici.ubicacion = [req.body.lat, req.body.lng];
    res.redirect('/bicicletas');
}

exports.bicicleta_delete_post = function(req, res){
    Bicicleta.removeById(req.body.id);
    res.redirect('/bicicletas');
}