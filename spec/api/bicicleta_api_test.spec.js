var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');//cuando lo exporta lo ejecuta, por ende no hace falta iniciar o finalizar el server

var base_url = 'http://localhost:3000/api/bicicletas';

describe('Bicicleta API', ()=>{
    describe('GET bicicletas', ()=>{
        it('status 200', ()=>{
            expect(Bicicleta.allBicis.length).toBe(0);
            var a = new Bicicleta(1, "verde", "motorizada");
            Bicicleta.add(a);

            //tiene que estar activo el servidor
            request.get(base_url, function(error, response, body){//ejecuta unn request
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe('POST bicicletas /create', ()=>{
        it('status 200', (done)=>{//done es un callback
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"code": 10, "color": "rosa", "modelo": "kawai", "lat": -34, "lng": -54 }';
            request.post({
                    headers: headers,
                    url: base_url+'/create',
                    body: aBici
                }, function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    expect(Bicicleta.findById(10).color).toBe("rosa");
                    done();//jasmin espera la ejecucion para que termine, o timeout
                    //como es asincronico POST request puede ser que termine sin obtener el resultado 
                    //y el done sirve para que se espere a obtener el request
                }
            );
        });
    });

    describe('DELETE bicicletas /delete', ()=>{
        
        it('status 204', (done)=>{
            expect(Bicicleta.allBicis.length).toBe(0);
            var a = new Bicicleta(1, "verde", "motorizada");
            Bicicleta.add(a);

            var headers = {'content-type' : 'application/json'};
            var aBici = '{"id": 1}';
            request.delete({
                    headers: headers,
                    url: base_url+'/delete',
                    body: aBici
                }, function(error, response, body){
                    expect(response.statusCode).toBe(204);//204 por que no devuelve una entidad
                    expect(Bicicleta.allBicis.length).toBe(0);
                    done();//jasmin espera la ejecucion para que termine, o timeout
                    //como es asincronico POST request puede ser que termine sin obtener el resultado 
                    //y el done sirve para que se espere a obtener el request
                }
            );
        });
    });

    describe('UPDATE bicicletas /:id/update', ()=>{
        it('status 200', (done)=>{
            expect(Bicicleta.allBicis.length).toBe(0);
            var a = new Bicicleta(1, "verde", "motorizada");
            Bicicleta.add(a);

            var headers = {'content-type' : 'application/json'};
            var aBici = '{"id": 3, "color": "rosa", "modelo": "kawai", "lat": -34, "lng": -54 }';
            request.put({
                    headers: headers,
                    url: base_url+'/1/update',
                    body: aBici
                }, function(error, response, body){
                    expect(response.statusCode).toBe(200);//204 por que no devuelve una entidad
                    expect(Bicicleta.findById(3).color).toBe("rosa");
                    done();//jasmin espera la ejecucion para que termine, o timeout
                    //como es asincronico POST request puede ser que termine sin obtener el resultado 
                    //y el done sirve para que se espere a obtener el request
                }
            );
        });
    });
});

//para las siguientes pruebas hace falta anidar 
